use aes::cipher::generic_array::GenericArray;
use aes::{Aes256, NewBlockCipher};
use std::fs::File;
use std::io;
use std::io::{Read, Seek, SeekFrom, Write};
use xts_mode::{get_tweak_default, Xts128};

pub trait FileLike: Read + Write + Seek {}

impl FileLike for File {}

pub struct EncryptedFile {
    file: Box<dyn FileLike>,
    xts: Xts128<Aes256>,
    sector_size: usize,
}

impl EncryptedFile {
    pub fn new(mut file: Box<dyn FileLike>, key: [u8; 64], sector_size: usize) -> EncryptedFile {
        let cipher_1 = Aes256::new(GenericArray::from_slice(&key[..32]));
        let cipher_2 = Aes256::new(GenericArray::from_slice(&key[32..]));

        if file.stream_position().unwrap() < sector_size as u64 {
            file.seek(SeekFrom::Start(sector_size as u64));
        }

        let xts = Xts128::<Aes256>::new(cipher_1, cipher_2);
        EncryptedFile {
            file,
            xts,
            sector_size,
        }
    }
}

impl Seek for EncryptedFile {
    fn seek(&mut self, pos: SeekFrom) -> io::Result<u64> {
        match pos {
            SeekFrom::Start(v) => self.file.seek(SeekFrom::Start(v + self.sector_size as u64)),
            SeekFrom::End(_) => self.file.seek(pos),
            SeekFrom::Current(_) => self.file.seek(pos),
        }
    }

    fn rewind(&mut self) -> io::Result<()> {
        self.file.rewind()
    }

    fn stream_position(&mut self) -> io::Result<u64> {
        self.file
            .stream_position()
            .and_then(|res| Ok(res - self.sector_size as u64))
    }
}

impl Read for EncryptedFile {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        // Needed calculations
        let initial_position = self.file.stream_position()?;
        let start_sector = initial_position / (self.sector_size as u64);
        let sector_offset =
            (initial_position % (self.sector_size as u64)) as usize + self.sector_size;
        // TODO: Potential overflow
        let read_sector_count = (buf.len() + sector_offset as usize) / (self.sector_size) + 1;
        let read_length = read_sector_count * (self.sector_size as usize);
        let relevant_end = buf.len() + sector_offset;
        let overshoot = read_length - relevant_end;

        // TODO: Handle file length

        // Content storage
        let mut content_vec = vec![0u8; read_length];
        let content = content_vec.as_mut_slice();

        // Read plaintext
        // TODO: Potential overflow
        self.file.seek(SeekFrom::Current(-(sector_offset as i64)))?;
        let read_size = self.file.read(content)?;
        if read_size < read_length {
            self.xts.decrypt_area(
                &mut content[..read_size],
                self.sector_size,
                start_sector as u128,
                get_tweak_default,
            );
        } else {
            // TODO: Potential overflow
            self.file.seek(SeekFrom::Current(-(overshoot as i64)))?;

            // Decrypt
            self.xts.decrypt_area(
                content,
                self.sector_size,
                start_sector as u128,
                get_tweak_default,
            );

            // File should only seek as it normally would
            debug_assert_eq!(
                initial_position as u64 + sector_offset as u64 - read_size as u64,
                self.file.stream_position().unwrap()
            );
        }

        // Return
        buf.copy_from_slice(&content[sector_offset..read_size]);

        Ok(read_size)
    }
}

impl Write for EncryptedFile {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        // Needed calculations
        let initial_position = self.file.stream_position()?;
        let start_sector = initial_position / (self.sector_size as u64);
        let sector_offset =
            (initial_position % (self.sector_size as u64)) as usize + self.sector_size;
        // TODO: Potential overflow
        let read_sector_count = (buf.len() + sector_offset) / (self.sector_size) + 1;
        let read_length = read_sector_count * (self.sector_size as usize);
        let relevant_end = buf.len() + sector_offset;
        let overshoot = read_length - relevant_end;

        let mut content_vec = vec![];

        let mut initial_skip = 0usize;
        if initial_position < (self.sector_size * 2) as u64 {
            initial_skip = self.sector_size * 2 - initial_position as usize;
            self.file.seek(SeekFrom::Current(initial_skip as i64))?;
            content_vec.append(&mut vec![0u8; initial_skip]);
        }

        // Read everything in from of the segment if there is something and add it to
        // plaintext sectors
        let mut before_vec = vec![0u8; sector_offset - initial_skip];
        let before = before_vec.as_mut_slice();

        // TODO: Potential overflow
        // Current fd pos: |---+-----|---|
        self.file.seek(SeekFrom::Current(-(sector_offset as i64)))?;
        // Current fd pos: +---|-----|---|
        let read_size = self.read(before)?;
        // Current fd pos: |---+-----|---|
        debug_assert_eq!(read_size, sector_offset);

        content_vec.append(&mut before_vec);

        // Add plaintext segment to plaintext sectors
        content_vec.append(&mut buf.to_vec());

        // Skip the part, where the segment will be written
        // TODO: Potential overflow
        self.file.seek(SeekFrom::Current(buf.len() as i64))?;
        // Current fd pos: |---|-----+---|
        let mut actual_overshoot: usize = 0;

        let pos_before = self.file.stream_position()?;
        self.file.seek(SeekFrom::End(0));
        let pos_end = self.file.stream_position()?;
        self.file.seek(SeekFrom::Start(pos_before))?;

        // If the skipped segment was fully present, read what's after in the sector and add it to
        // the plaintext, afterwards reset the fd to the beginning of the affected sectors
        if pos_end > pos_before {
            // Current fd pos: |---|-----+---|
            let mut after_vec = vec![0u8; overshoot];
            let after = after_vec.as_mut_slice();

            actual_overshoot = self.read(after)?;
            // Current fd pos: |---|-----|--+| or |---|-----|---+
            content_vec.append(&mut after[..actual_overshoot].to_vec());
            self.file.seek(SeekFrom::Current(
                -((relevant_end + actual_overshoot) as i64),
            ))?;
        } else if pos_end < pos_before {
            // Current fd pos: |---|--+--|---|
            // TODO: Potential overflow
            self.file.seek(SeekFrom::Current(
                -((sector_offset as u64 + (pos_before - pos_end)) as i64),
            ))?;
        }
        // Current fd pos: +---|-----|---|

        // Encrypt data
        let content = content_vec.as_mut_slice();
        self.xts.encrypt_area(
            content,
            self.sector_size,
            start_sector as u128,
            get_tweak_default,
        );

        // Write data
        let tst = self.file.stream_position()?;
        self.file.write(content)?;

        // If we wrote something after the segment, reset the fd to the end of the segment
        if actual_overshoot != 0 {
            // Current fd pos: |---|-----|--+| or |---|-----|---+
            self.file
                .seek(SeekFrom::Current(-(actual_overshoot as i64)))?;
        }
        // Current fd pos: |---|-----+---|

        Ok(buf.len())
    }

    fn flush(&mut self) -> io::Result<()> {
        self.file.flush()
    }
}
